SPRUCE (Specialized Probabilistic Renovation of UnClean Entites) - an algorithm for cleaning up tabular data.

SPRUCE uses a probabilistic model informed by common errors prevent in raw data to infer what the cleaned-up version of some data will look like. This model can be pre-trained on unrelated datasets.
